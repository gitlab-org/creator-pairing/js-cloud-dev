var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('myip', { myIp: req.headers['x-forwarded-for'] });
});

module.exports = router;
