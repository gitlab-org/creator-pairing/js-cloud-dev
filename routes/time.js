var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('time', { time: (new Date().toLocaleString()) });
});

module.exports = router;
